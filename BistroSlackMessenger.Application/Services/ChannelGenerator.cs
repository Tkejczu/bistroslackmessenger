﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using BistroSlackMessenger.Application.Boundary.BistroApiModels;
using BistroSlackMessenger.Application.Boundary.Services;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace BistroSlackMessenger.Application.Services
{
    public class ChannelGenerator : IChannelGenerator
    {
        public string GenerateChannel(string userId)
        {
            using (var client = new HttpClient())
            {
                var parameters = new FormUrlEncodedContent(new List<KeyValuePair<string, string>>()
                {
                    new KeyValuePair<string, string>("token",
                        "xoxp-427406754593-429020447271-431249251239-baa89f1e8dfe57f0c36e4a632bb5cbc0"),
                    new KeyValuePair<string, string>("users", userId)
                });
                var response = client.PostAsync(new Uri("https://slack.com/api/conversations.open"), parameters).Result;

                var channel = response.Content.ReadAsStringAsync().Result;

                return JsonConvert.DeserializeObject<SlackChannelResponse>(channel).Id;

            }
           
        }
    }
}
