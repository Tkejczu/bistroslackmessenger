﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace BistroSlackMessenger.Application.Boundary.BistroApiModels
{
    public class User
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }
    }
}
