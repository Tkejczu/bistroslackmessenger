﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BistroSlackMessenger.Application.Boundary;
using BistroSlackMessenger.Application.Boundary.BistroApiModels;
using BistroSlackMessenger.Application.Boundary.Configurations;
using BistroSlackMessenger.Application.Boundary.Services;

namespace BistroSlackMessenger.Application.Services
{
    public class PrivateMessageExecution : IPrivateMessageExecution
    {
        private readonly IBistroApiClient _bistroApiClient;
        private readonly ISlackApiClient _slackApiClient;
        private readonly IOrderChecker _orderChecker;
        private readonly IChannelGenerator _channelGenerator;
        private readonly IMessageGenerator _messageGenerator;
        private readonly IMessageSender _messageSender;

        public PrivateMessageExecution(
            IBistroApiClient bistroApiClient, 
            ISlackApiClient slackApiClient, 
            IOrderChecker orderChecker,
            IChannelGenerator channelGenerator,
            IMessageGenerator messageGenerator,
            IMessageSender messageSender)
        {
            _bistroApiClient = bistroApiClient;
            _slackApiClient = slackApiClient;
            _orderChecker = orderChecker;
            _channelGenerator = channelGenerator;
            _messageGenerator = messageGenerator;
            _messageSender = messageSender;
        }

        public void ExecutePrivateMessage()
        {
            var orders = _bistroApiClient.GetOrdersForDay(new DateTime(2018,08,31));

            var users = _slackApiClient.GetUsers();

            var usersWithOrders = _orderChecker.SetSlackUsersToOrders(users, orders);


            foreach (var userOrder in usersWithOrders)
            {
                var openChannel = _channelGenerator.GenerateChannel(userOrder.User.Id);

                var message = _messageGenerator.CreatePrivateMessage(usersWithOrders.FirstOrDefault());

                _messageSender.SendPrivateMessage(openChannel, message).Wait();
            }
        }
    }
}