﻿using System;
using System.Collections.Generic;
using BistroSlackMessenger.Application.Boundary.BistroApiModels;
using BistroSlackMessenger.Application.Services;
using Xunit;

namespace BistroSlackMessenger.Application.Tests
{
    public class MessageGeneratorTests
    {
        [Fact]
        public void Message_should_be_same_as_expected()
        {
            var menu = new Menu()
            {
                Date = new DateTime(2018, 9, 6),
                AvailableDate = new DateTime(2018, 9, 5),
                LimitedOrdersAvailableDate = new DateTime(2018, 9, 6),
                MenuDishes = new[]
                {
                    new MenuDish()
                    {
                        Position = 1,
                        Price = 4,
                        Dish = new Dish()
                        {
                            Name = "Zupa ogórkowa zabielana z koperkiem i ryżem"
                        }
                    },
                    new MenuDish()
                    {
                        Position = 2,
                        Price = 11,
                        Dish = new Dish()
                        {
                            Name = "Kurczak pieczony, ziemniaki, sałatka lodowa z sosem winegret"
                        }
                    },
                    new MenuDish()
                    {
                        Position = 3,
                        Price = 11,
                        Dish = new Dish()
                        {
                            Name = "Leczo z mięsem i ryżem"
                        }
                    }
                }
            };


            var generator = new MessageGenerator();

            var message = generator.CreateMessage(menu);

            var expectedMessage =
                "Dzień dobry, bistro menu na dzień dzisiejszy (2018-09-06):\n" +
                "Data nielimitowanej dostępności: 2018-09-05\n" +
                "Data limitowanej dostępności: 2018-09-06\n" +
                "Nr 1 Danie: Zupa ogórkowa zabielana z koperkiem i ryżem Cena: 4,00 zł\n" +
                "Nr 2 Danie: Kurczak pieczony, ziemniaki, sałatka lodowa z sosem winegret Cena: 11,00 zł\n" +
                "Nr 3 Danie: Leczo z mięsem i ryżem Cena: 11,00 zł\n";

            Assert.Equal(expectedMessage, message);
        }

        [Fact]
        public void private_message_should_be_same_as_expected()
        {
            var ordersPerUser = new SlackUserOrder()
            {
                User = new User()
                {
                    Email = "Test@gmail.com",
                    Id = "12345532",
                    Name = "ExampleUser"
                },
                Order = new Order()
                {
                    Date = new DateTime(2018,9,10),
                    
                    OrderItems = new OrderItem[]
                    {
                        
                        new OrderItem()
                        {
                            Quantity = 4,
                            MenuDish = new MenuDish()
                            
                            {
                                Price = 123,
                                Dish = new Dish()
                                {
                                    Name = "Flaki z olejem"
                                }
                            }
                        }
                    }
                    
                }
            };

            var generator = new MessageGenerator();

            var privateMessage = generator.CreatePrivateMessage(ordersPerUser);

            var expectedPrivateMessage =
                $"Witaj ExampleUser, przypomnienie o Twoim dzisiejszym zamówieniu:\n" +
                $"Danie: Flaki z olejem\n" +
                $"Ilość: 4\n" +
                $"Cena/szt: 123,00 zł\n" +
                $"Życzymy smacznego!";

            Assert.Equal(expectedPrivateMessage, privateMessage);
        }
    }
}