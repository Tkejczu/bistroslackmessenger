﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BistroSlackMessenger.Application.Boundary.BistroApiModels
{
    public class SlackUserOrder
    {
        public User User { get; set; }
        public Order Order { get; set; }
    }
}
