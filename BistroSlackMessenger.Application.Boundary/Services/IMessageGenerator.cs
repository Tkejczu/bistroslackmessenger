﻿using BistroSlackMessenger.Application.Boundary.BistroApiModels;

namespace BistroSlackMessenger.Application.Boundary.Services
{
    public interface IMessageGenerator
    {
        string CreateMessage(Menu menu);
        string CreatePrivateMessage(SlackUserOrder user);
    }
}