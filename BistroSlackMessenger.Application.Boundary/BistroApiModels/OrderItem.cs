﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace BistroSlackMessenger.Application.Boundary.BistroApiModels
{
    public class OrderItem
    {
        public int OrderItemId { get; set; }
        public int OrderId { get; set; }
        public int MenuDishId { get; set; }
        public int Quantity { get; set; }
        public MenuDish MenuDish{ get; set; }
    }
}
