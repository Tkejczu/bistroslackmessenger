﻿using System;
using System.Collections.Generic;
using System.Text;
using BistroSlackMessenger.Application.Boundary.BistroApiModels;
using BistroSlackMessenger.Application.Boundary.Configurations;
using BistroSlackMessenger.Application.Services;
using Newtonsoft.Json.Linq;
using Xunit;

namespace BistroSlackMessenger.Application.Tests
{

    public class BistroApiClientExpensiveTests
    {
        [Fact]
        public void Should_get_menu_for_day()
        {
            var configuration = new BistroApiConfiguration()
            {
                BistroApiUrl = "http://pkp-srv/BistroApp/api/v1/"
            };

            var day = new DateTime(2018, 9, 5);

            var client = new BistroApiClient(configuration);

            var menu = client.GetMenu(day);

            Assert.NotNull(menu);
            Assert.Equal(152, menu.Id);
        }
    }
}
