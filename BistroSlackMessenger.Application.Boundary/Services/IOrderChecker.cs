﻿using System;
using System.Collections.Generic;
using System.Text;
using BistroSlackMessenger.Application.Boundary.BistroApiModels;

namespace BistroSlackMessenger.Application.Boundary.Services
{
    public interface IOrderChecker
    {
        IEnumerable<SlackUserOrder> SetSlackUsersToOrders(IEnumerable<User> slackUsers, IEnumerable<Order> orders);
    }
}
