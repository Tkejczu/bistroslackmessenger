﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using BistroSlackMessenger.Application.Boundary.BistroApiModels;
using BistroSlackMessenger.Application.Boundary.Configurations;
using BistroSlackMessenger.Application.Services;
using Newtonsoft.Json.Linq;
using Xunit;
using System.Reflection;
using BistroSlackMessenger.Application.Boundary.Services;
using NSubstitute;


namespace BistroSlackMessenger.Application.Tests

{
    public class OrderCheckerTests
    {
        [Fact]
        public void When_found_user_Should_set_it_in_order()
        {
            var users = new[]
            {
                new User()
                {
                    Id = "1",
                    Email = "test@foo.com"
                },
                new User()
                {
                    Id = "2",
                    Email = "abc@foo.com"
                }
            };

            var orders = new[]
            {
                new Order()
                {
                    User = new User()
                    {
                        Email = "test@foo.com"
                    }
                }, 
            };

            var checker = new OrderChecker();

            var slackUserOrders = checker.SetSlackUsersToOrders(users, orders).ToArray();

            Assert.Single(slackUserOrders);
            Assert.Equal("test@foo.com", slackUserOrders.FirstOrDefault()?.User.Email);
        }

        [Fact]
        public void Should_get_users()
        {
            var api = new SlackApiClient(new SlackApiConfiguration());

            var test = api.GetUsers();
           
            Assert.NotNull(test);   
        }
    }
}
