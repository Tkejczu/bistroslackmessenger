﻿using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Slack.Webhooks;

namespace BistroSlackMessenger.Application.Boundary.Services
{
    public interface IMessageSender
    {
        string SendMenuMessage();
        Task<HttpResponseMessage> SendPrivateMessage(string channel, string privateMessage);
    }
}
