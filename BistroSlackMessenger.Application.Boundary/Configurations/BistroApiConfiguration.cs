﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BistroSlackMessenger.Application.Boundary.Configurations
{
    public class BistroApiConfiguration
    {
        public string BistroApiUrl { get; set; }
    }
}
