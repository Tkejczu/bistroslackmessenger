﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;
using BistroSlackMessenger.Application.Boundary.Configurations;
using Newtonsoft.Json.Linq;
using Xunit;
using BistroSlackMessenger.Application;
using BistroSlackMessenger.Application.Boundary;
using BistroSlackMessenger.Application.Services;
using Slack.Webhooks;

namespace BistroSlackMessenger.Application.Tests
{

    public class SlackApiClientPostMessageTests
    {
      
        [Fact]
        public void Message_should_be_sent()
        {
            var configuration = new SlackApiConfiguration()
            {
                UrlWithAccessToken = "https://hooks.slack.com/services/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"
            };

            var client = new SlackApiClient(configuration);

            client.PostMessage("#bistrochannel", "AaaaaaaaaaaaAAAAAAAAAAAAAAAAAAAAAAaaaaaaaaaa", "BistroBot");

        }
    }
}