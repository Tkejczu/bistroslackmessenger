﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using BistroSlackMessenger.Application.Boundary.BistroApiModels;
using BistroSlackMessenger.Application.Boundary.Configurations;
using BistroSlackMessenger.Application.Boundary.Services;
using Newtonsoft.Json;

namespace BistroSlackMessenger.Application.Services
{
    public class BistroApiClient : IBistroApiClient
    {
        private readonly BistroApiConfiguration _bistroApiConfiguration;

        public BistroApiClient(BistroApiConfiguration bistroApiConfiguration)
        {
            _bistroApiConfiguration = bistroApiConfiguration;
        }

        public Menu GetMenu(DateTime day)
        {
            using (var client = new HttpClient())
            {
                var response = client.GetStringAsync(new Uri(new Uri(_bistroApiConfiguration.BistroApiUrl), "menu/" + day.ToString("yyyy-MM-dd"))).Result;


                return JsonConvert.DeserializeObject<Menu>(response);
            }
        }

        public IEnumerable<Order> GetOrdersForDay(DateTime menuDate)
        {
            throw new NotImplementedException();
        }
    }
}
