﻿using System.Globalization;
using System.Linq;
using BistroSlackMessenger.Application.Boundary.BistroApiModels;
using BistroSlackMessenger.Application.Boundary.Services;

namespace BistroSlackMessenger.Application.Services
{
    public class MessageGenerator : IMessageGenerator
    {
        public string CreateMessage(Menu menu)
        {
            var msg = $"Dzień dobry, bistro menu na dzień dzisiejszy ({menu.Date.ToString("yyyy-MM-dd")}):\n" +
                      $"Data nielimitowanej dostępności: {menu.AvailableDate.ToString("yyyy-MM-dd")}\n" +
                      $"Data limitowanej dostępności: {menu.LimitedOrdersAvailableDate.ToString("yyyy-MM-dd")}\n";

            return menu.MenuDishes
                .Select(n => $"Nr {n.Position} Danie: {n.Dish.Name} Cena: {n.Price.ToString("C", CultureInfo.GetCultureInfo("pl-PL"))}\n")
                .Aggregate(msg, (context, n) => context + n);           
        }
        public string CreatePrivateMessage(SlackUserOrder user)
        {
            var pmsg = $"Witaj {user.User.Name}, przypomnienie o Twoim dzisiejszym zamówieniu:\n";
                

            return user.Order.OrderItems
                .Select(n => $"Danie: {n.MenuDish.Dish.Name}\nIlość: {n.Quantity}\nCena/szt: {n.MenuDish.Price.ToString("C", CultureInfo.GetCultureInfo("pl-PL"))}")
                .Aggregate(pmsg, (context, n) => context + n + "\nŻyczymy smacznego!");
        }
    }
}