﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BistroSlackMessenger.Application.Boundary.Configurations
{
    public class SlackApiConfiguration
    {
        public string UrlWithAccessToken { get; set; }
    }
}
