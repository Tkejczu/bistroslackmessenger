﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;


namespace BistroSlackMessenger.Application.Boundary.BistroApiModels
{
    public class Order
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public int MenuId { get; set; }
        public bool IsTakeout { get; set; }
        public IEnumerable<OrderItem> OrderItems  { get; set; }
        public User User { get; set; }
        public Menu Menu { get; set; }
        public DateTime Date { get; set; }
    }
}
