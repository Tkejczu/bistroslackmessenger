﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using BistroSlackMessenger.Application.Boundary.BistroApiModels;
using Newtonsoft.Json;

namespace BistroSlackMessenger.Application.Services
{
    public class OrderParser
    {
        public IEnumerable<Order> ParseOrder(string json)
        {
               IEnumerable<Order> convertOrders = JsonConvert.DeserializeObject<IEnumerable<Order>>(json);
            return convertOrders;
        }
    }
}

