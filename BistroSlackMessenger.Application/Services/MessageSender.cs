﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using BistroSlackMessenger.Application.Boundary.BistroApiModels;
using BistroSlackMessenger.Application.Boundary.Configurations;
using BistroSlackMessenger.Application.Boundary.Services;
using Slack.Webhooks;

namespace BistroSlackMessenger.Application.Services
{
    public class MessageSender : IMessageSender
    {
        private readonly IBistroApiClient _bistroApiClient;
        private readonly ISlackApiClient _slackApiClient;
        private readonly IMessageGenerator _messageGenerator;

        public MessageSender(
            IBistroApiClient bistroApiClient,
            ISlackApiClient slackApiClient,
            IMessageGenerator messageGenerator)
        {
            _bistroApiClient = bistroApiClient;
            _slackApiClient = slackApiClient;
            _messageGenerator = messageGenerator;
        }

        public string SendMenuMessage()
        {
            var menu = _bistroApiClient.GetMenu(DateTime.Now);

            var message = _messageGenerator.CreateMessage(menu);

            _slackApiClient.PostMessage("#bistrochannel", message, "BistroBot");

            return message;
        }

        public async Task<HttpResponseMessage> SendPrivateMessage(string channel, string privateMessage)
        {
            using (var client = new HttpClient())
            {
                var parameters = new FormUrlEncodedContent(new List<KeyValuePair<string, string>>()
                {
                new KeyValuePair<string, string>("token",
                        "xoxp-427406754593-429020447271-431249251239-baa89f1e8dfe57f0c36e4a632bb5cbc0"),
                    new KeyValuePair<string, string>("channel", channel),
                    new KeyValuePair<string, string>("text", privateMessage)
                });
                    var response = client.PostAsync(new Uri("https://slack.com/api/chat.postMessage"), parameters).Result;

                return response;
            }
        }
    }
}

