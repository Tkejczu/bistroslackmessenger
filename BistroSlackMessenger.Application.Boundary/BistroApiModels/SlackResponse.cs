﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace BistroSlackMessenger.Application.Boundary.BistroApiModels
{
    public class SlackResponse
    {
        [JsonProperty("ok")]
        public bool SlackApiResponse { get; set; }

        [JsonProperty("members")]
        public IEnumerable<User> Users { get; set; }
    }
}
