﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using BistroSlackMessenger.Application.Boundary.BistroApiModels;
using BistroSlackMessenger.Application.Boundary.Configurations;
using BistroSlackMessenger.Application.Boundary.Services;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Slack.Webhooks;

namespace BistroSlackMessenger.Application.Services
{
    public class SlackApiClient : ISlackApiClient
    {
        private readonly SlackApiConfiguration _slackApiConfiguration;

        public SlackApiClient(SlackApiConfiguration slackApiConfiguration)
        {
            _slackApiConfiguration = slackApiConfiguration;
        }

        public void PostMessage(string channel, string text, string username)
        {
            var slackClient = new SlackClient(_slackApiConfiguration.UrlWithAccessToken);

            var slackmessage = new SlackMessage()
            {
                Channel = channel,
                Text = text,
                IconEmoji = Emoji.Apple,
                Username = username
            };
            
            slackClient.Post(slackmessage);
        }

        public IEnumerable<User> GetUsers()
        {
            using (var client = new HttpClient())
            {
                var parameters = new FormUrlEncodedContent(new List<KeyValuePair<string, string>>()
                {
                    new KeyValuePair<string, string>("token", "xoxp-427406754593-429020447271-431249251239-baa89f1e8dfe57f0c36e4a632bb5cbc0")
                });

                var response = client.PostAsync(new Uri("https://slack.com/api/users.list"), parameters).Result;

                var users = response.Content.ReadAsStringAsync().Result;


                
                return JsonConvert.DeserializeObject<SlackResponse>(users).Users;
            }
        }

    }
}


    

