﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using BistroSlackMessenger.Application.Boundary.BistroApiModels;
using BistroSlackMessenger.Application.Boundary.Configurations;
using BistroSlackMessenger.Application.Services;
using ExpectedObjects;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Xunit;
using System.Reflection;
using ExpectedObjects.Strategies;

namespace BistroSlackMessenger.Application.Tests
{

    public class OrderParserTests
    {

        [Fact]
        public void Should_get_same_data_for_order()
        {
            var json = GetJsonContent(Assembly.GetExecutingAssembly(), "bistroapp.json");

            var expected =
                new Order()
                {
                    Id = 1394,
                    UserId = "550e9db6-402c-4788-9b7d-a4cb0ce4aa77",
                    MenuId = 150,
                    IsTakeout = false,
                    Date = new DateTime(2018, 08, 31, 15, 09, 27),
                    OrderItems = new List<OrderItem>()
                    {
                            new OrderItem()
                            {
                                OrderItemId = 1946,
                                OrderId = 1394,
                                MenuDishId = 544,
                                Quantity = 4,
                                MenuDish = new MenuDish()
                                {
                                    Id = 544,
                                    Position = 2,
                                    MenuId = 150,
                                    DishId = 544,
                                    Price = 11,
                                    LimitedOrdersValue = 5,
                                    LimitedOrdersCount = 0,
                                    Dish = new Dish()
                                    {
                                        Id = 544,
                                        Name = "Schab pieczony w sosie ziołowym, kluski śląskie , zestaw surówek"
                                    }
                                }
                            },
                            new OrderItem()
                            {
                                OrderItemId = 1947,
                                OrderId = 1394,
                                MenuDishId = 543,
                                Quantity = 1,
                                MenuDish = new MenuDish
                                {
                                    Id = 543,
                                    Position = 1,
                                    MenuId = 150,
                                    DishId = 543,
                                    Price = 4,
                                    LimitedOrdersValue = 7,
                                    LimitedOrdersCount = 0,
                                    Dish = new Dish
                                    {
                                        Id = 543,
                                        Name = "Zupa jarzynowa zabielana z zieleniną"
                                    }
                                }
                            }
                    },
                    User = new User()
                    {
                        Id = "550e9db6-402c-4788-9b7d-a4cb0ce4aa77",
                        Email = "anke12.anke12@gmail.com"
                    },
                    Menu = new Menu()
                    {
                        Id = 150,
                        Date = new DateTime(2018, 09, 03),
                        AvailableDate = new DateTime(2018, 09, 02, 16, 00, 0),
                        LimitedOrdersAvailableDate = new DateTime(2018, 09, 03, 11, 00, 00),
                        MenuDishes = new List<MenuDish>()
                        {
                                new MenuDish()
                                {
                                    Id = 544,
                                    Position = 2,
                                    MenuId = 150,
                                    DishId = 544,
                                    Price = 11,
                                    LimitedOrdersValue = 5,
                                    LimitedOrdersCount = 0,
                                    Dish = new Dish
                                    {
                                        Id = 544,
                                        Name = "Schab pieczony w sosie ziołowym, kluski śląskie , zestaw surówek"
                                    }
                                },
                                new MenuDish()
                                {
                                Id = 543,
                                Position = 1,
                                MenuId = 150,
                                DishId = 543,
                                Price = 4,
                                LimitedOrdersValue = 7,
                                LimitedOrdersCount = 0,
                                Dish = new Dish
                                {
                                    Id = 543,
                                    Name = "Zupa jarzynowa zabielana z zieleniną"
                                }
                                },
                                new MenuDish()
                                {
                                Id = 545,
                                Position = 3,
                                MenuId = 150,
                                DishId = 545,
                                Price = 11,
                                LimitedOrdersValue = 5,
                                LimitedOrdersCount = 0,
                                Dish = new Dish
                                {
                                    Id = 545,
                                    Name = "Knedle z owocami, masłem i cukrem pudrem"
                                }
                                }
                        }
                    }

                }.ToExpectedObject();

            var result = new OrderParser().ParseOrder(json).FirstOrDefault();

            expected.ShouldEqual(result);
        }



        private string GetJsonContent(Assembly assembly, string name)
        {
            var jsonPath = assembly.GetManifestResourceNames().SingleOrDefault(n => n.EndsWith(name));

            var stream = assembly.GetManifestResourceStream(jsonPath);

            using (var reader = new StreamReader(stream))
            {
                return reader.ReadToEnd();
            }
        }
    }

}

