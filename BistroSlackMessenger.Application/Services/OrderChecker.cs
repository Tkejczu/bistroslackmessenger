﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using BistroSlackMessenger.Application.Boundary.Configurations;
using BistroSlackMessenger.Application.Boundary.BistroApiModels;
using BistroSlackMessenger.Application.Boundary.Services;

namespace BistroSlackMessenger.Application.Services
{
    public class OrderChecker : IOrderChecker
    {
       
        public IEnumerable<SlackUserOrder> SetSlackUsersToOrders(IEnumerable<User> slackUsers, IEnumerable<Order> orders)
        {
            foreach (var order in orders)
            {
                var user = slackUsers.FirstOrDefault(n => n.Email == order.User.Email);

                if (user != null)
                {
                    yield return new SlackUserOrder()
                    {
                        User = user,
                        Order = order
                    };
                }
            }
        }
    }
}
