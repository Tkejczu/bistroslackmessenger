﻿using System;
using System.Collections.Generic;
using BistroSlackMessenger.Application.Boundary.BistroApiModels;

namespace BistroSlackMessenger.Application.Boundary.Services
{
    public interface IBistroApiClient
    {
        Menu GetMenu(DateTime day);
        IEnumerable<Order> GetOrdersForDay(DateTime menuDate);
    }
}