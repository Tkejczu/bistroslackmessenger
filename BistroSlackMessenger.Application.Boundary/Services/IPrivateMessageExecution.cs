﻿namespace BistroSlackMessenger.Application.Boundary.Services
{
    public interface IPrivateMessageExecution
    {
        void ExecutePrivateMessage();
    }
}