﻿using System;
using System.Collections.Generic;
using System.Text;
using BistroSlackMessenger.Application.Boundary.Services;
using BistroSlackMessenger.Application.Services;
using Microsoft.Extensions.DependencyInjection;

namespace BistroSlackMessenger.Application
{
    public static class ApplicationDependency
    {
        public static void AddApplication(this IServiceCollection services)
        {
            services.AddTransient<IBistroApiClient, FakeBistroApiClient>();
            services.AddTransient<IMessageGenerator, MessageGenerator>();
            services.AddTransient<IMessageSender, MessageSender>();
            services.AddTransient<ISlackApiClient, SlackApiClient>();
            services.AddTransient<IPrivateMessageExecution, PrivateMessageExecution>();
            services.AddTransient<IOrderChecker, OrderChecker>();
            services.AddTransient<IChannelGenerator, ChannelGenerator>();
        }
    }
}
