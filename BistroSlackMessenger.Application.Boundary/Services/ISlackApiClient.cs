﻿using System.Collections.Generic;
using BistroSlackMessenger.Application.Boundary.BistroApiModels;

namespace BistroSlackMessenger.Application.Boundary.Services
{
    public interface ISlackApiClient
    {
        void PostMessage(string channel, string text, string username);
        IEnumerable<User> GetUsers();
    }
}