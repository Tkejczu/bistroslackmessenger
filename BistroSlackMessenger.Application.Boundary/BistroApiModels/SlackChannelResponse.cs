﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace BistroSlackMessenger.Application.Boundary.BistroApiModels
{
    public class SlackChannelResponse
    {
        [JsonProperty("ok")]
        public bool ChannelResponse { get; set; }

        [JsonProperty("id")]
        public string Id { get; set; }

    }
}
