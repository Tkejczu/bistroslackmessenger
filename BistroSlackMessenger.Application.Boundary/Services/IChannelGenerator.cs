﻿namespace BistroSlackMessenger.Application.Boundary.Services
{
    public interface IChannelGenerator
    {
        string GenerateChannel(string userId);
    }
}