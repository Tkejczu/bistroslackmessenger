﻿using System;
using System.Collections.Generic;
using BistroSlackMessenger.Application.Boundary.BistroApiModels;
using BistroSlackMessenger.Application.Boundary.Services;

namespace BistroSlackMessenger.Application.Services
{
    public class FakeBistroApiClient : IBistroApiClient
    {
        public Menu GetMenu(DateTime day)
        {
            return new Menu();
        }

        public IEnumerable<Order> GetOrdersForDay(DateTime menuDate)
        {
            return new[]
            {
                new Order()
                {
                    Id = 1394,
                    UserId = "550e9db6-402c-4788-9b7d-a4cb0ce4aa77",
                    MenuId = 150,
                    IsTakeout = false,
                    OrderItems = new[]
                    {
                        new OrderItem()
                        {
                            OrderItemId = 1946,
                            OrderId = 1394,
                            MenuDishId = 544,
                            Quantity = 4,
                            MenuDish = new MenuDish()
                            {
                                Id = 544,
                                Position = 2,
                                MenuId = 150,
                                DishId = 544,
                                Price = 11,
                                LimitedOrdersValue = 5,
                                LimitedOrdersCount = 0,
                                Dish= new Dish()
                                {
                                    Id  = 544,
                                    Name = "Schab pieczony w sosie ziołowym, kluski śląskie , zestaw surówek"
                                },
                            }
                        },
                        new OrderItem()
                        {
                            OrderItemId = 1947,
                            OrderId = 1394,
                            MenuDishId = 543,
                            Quantity = 1,
                            MenuDish = new MenuDish()
                            {
                                Id = 543,
                                Position = 1,
                                MenuId = 150,
                                DishId = 543,
                                Price = 4,
                                LimitedOrdersValue = 7,
                                LimitedOrdersCount = 0,
                                Dish = new Dish()
                                {
                                    Id = 543,
                                    Name = "Zupa jarzynowa zabielana z zieleniną"
                                },
                            },
                        },
                    },
                    User = new User()
                    {
                        Id = "550e9db6-402c-4788-9b7d-a4cb0ce4aa77",
                        Email = "tkacz.kam@gmail.com"
                    },
                    //  "date":"2018-08-31T15:09:27.0+02:00"
                    Date = menuDate
                },
            };

        }
    }
}