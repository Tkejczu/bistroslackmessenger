﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BistroSlackMessenger.Application.Boundary.BistroApiModels
{
    public class Menu
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public DateTime AvailableDate { get; set; }
        public DateTime LimitedOrdersAvailableDate { get; set; }
        public IEnumerable<MenuDish> MenuDishes { get; set; }
    }
}
