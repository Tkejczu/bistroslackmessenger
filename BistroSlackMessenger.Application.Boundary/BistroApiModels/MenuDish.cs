﻿namespace BistroSlackMessenger.Application.Boundary.BistroApiModels
{
    public class MenuDish
    {
        public int Id { get; set; }
        public int Position { get; set; }
        public int MenuId { get; set; }
        public int DishId { get; set; }
        public decimal Price { get; set; }
        public int LimitedOrdersValue { get; set; }
        public int LimitedOrdersCount { get; set; }
        public Dish Dish { get; set; }
    }
}