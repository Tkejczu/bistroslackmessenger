﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BistroSlackMessenger.Application;
using BistroSlackMessenger.Application.Boundary.Configurations;
using BistroSlackMessenger.Application.Boundary.Services;
using BistroSlackMessenger.Application.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Clients.ActiveDirectory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Configuration.Binder;
using Microsoft.Extensions.Configuration.Json;
using Hangfire;
using Hangfire.MemoryStorage;


namespace BistroSlackMessenger
{
    public class Startup
    {
        public IConfiguration Configuration { get; set; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;

        }

        public void ConfigureServices(IServiceCollection services)
        {
            AddConfiguration(services);
            services.AddHangfire(x => x.UseMemoryStorage());
            services.AddApplication();
        }

        private void AddConfiguration(IServiceCollection services)
        {
            var bistroConfig = new BistroApiConfiguration();
            var slackConfig = new SlackApiConfiguration();

            Configuration.Bind("BistroApiConfiguration", bistroConfig);
            Configuration.Bind("SlackApiConfiguration", slackConfig);

            GlobalConfiguration.Configuration.UseMemoryStorage();

            services.AddSingleton(bistroConfig);
            services.AddSingleton(slackConfig);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHangfireServer();
            app.UseHangfireDashboard();
            RecurringJob.AddOrUpdate("SendMenuMsg", () => app.ApplicationServices.GetService<IMessageSender>().SendMenuMessage(), "30 7 * * *");
            
            app.Run(async (context) =>
            {
                app.ApplicationServices.GetService<IPrivateMessageExecution>().ExecutePrivateMessage();


            });
        }
    }
}
